# 3088 PiHat Project

# Project Description of the PiHat which is an Uninterrupted Power Supply (UPS)

When plugged into any source, the microPiHat design acts as an Uninterruptible Power Supply (UPS) using intelligent power management to keep the Raspberry Pi Zero computer powered on. The PiHat will also stay charged, this will allow mobility of the Raspberry PiZero and projects can be done on the go on the Raspberry PiZero. This is a pefect and smart way of adding battery usage and smart power management to the Raspberry Pi Zero.

# How it will work

It will consist of the following subsystems which will provide a user-friendly interface for functionality:
1.	Voltage regulator: This will be responsible for power circuitry to convert battery voltage to correct Pi voltages.

2.	Voltmeter” or Shunt and signal op-amp constraining values to 0-3.3V This will help to decide when to shut the Pi down as this subsystem will determine on how full the battery is.

3.	Status LEDs Showing battery status e.g. Battery Voltage above 80%V to show ‘full’, or just an indicator showing “using battery power / not” or an indicator warning of low voltage
